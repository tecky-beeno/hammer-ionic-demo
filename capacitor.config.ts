import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'hammer-ionic-demo',
  webDir: 'build',
  bundledWebRuntime: false
};

export default config;
