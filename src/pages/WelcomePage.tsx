import { IonContent, IonPage, useIonRouter } from '@ionic/react'
import Hammer from 'hammerjs'
import { useLocation } from 'react-router'
import { dispatch, useEvent } from 'react-use-event'

let hammer = new Hammer(document.body)

type SwipeEvent = {
  type: 'custom-swipe'
} & TouchInput

let events = ['pan', 'swipe']
for (let type of events) {
  hammer.on(type, ev => {
    // console.log('global event:', ev)
    let data = {} as any
    let skipFields = ['timeStamp', 'target', 'type']
    for (let key in ev) {
      if (!skipFields.includes(key)) {
        data[key] = (ev as any)[key]
      }
    }
    dispatch<SwipeEvent>('custom-swipe', data)
  })
}

const WelcomePage: React.FC = () => {
  const router = useIonRouter()
  const location = useLocation()
  useEvent<SwipeEvent>('custom-swipe', event => {
    // console.log('react event:', event)
    // console.log('location:', location)
    if (location.pathname !== '/') {
      return
    }
    if (
      event.direction === Hammer.DIRECTION_LEFT ||
      event.direction === Hammer.DIRECTION_UP
    ) {
      // router.push('/tab1', 'forward', 'replace')
      router.push('/tab1', 'forward')
    }
  })
  return (
    <IonPage>
      <IonContent fullscreen>
        <p>Welcome</p>
      </IonContent>
    </IonPage>
  )
}

export default WelcomePage
